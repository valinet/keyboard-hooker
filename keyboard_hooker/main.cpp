#include <D:\Valentin\Documente\Visual Studio 2015\Projects\keyboard_hooker\keyboard_hooker\interception.h>

#include <string>
#include <iostream>
#include <cstdio>
#include <memory>
#include <fstream>
#include <ctime>
#include <Windows.h>

#include <D:\Valentin\Documente\Visual Studio 2015\Projects\keyboard_hooker\keyboard_hooker\utils.h>


using namespace std;

enum ScanCode
{
	SCANCODE_X = 0x2D,
	SCANCODE_Y = 0x15,
	SCANCODE_ESC = 0x01
};

std::string exec(const char* cmd) {
	std::shared_ptr<FILE> pipe(_popen(cmd, "r"), _pclose);
	if (!pipe) return "ERROR";
	char buffer[128];
	std::string result = "";
	while (!feof(pipe.get())) {
		if (fgets(buffer, 128, pipe.get()) != NULL)
			result += buffer;
	}
	return result;
}


string GetActiveWindowTitle()
{
	char wnd_title[256];
	HWND hwnd = GetForegroundWindow(); // get handle of currently active window
	GetWindowTextA(hwnd, wnd_title, sizeof(wnd_title));
	return wnd_title;
}

string GetCursorWindowTitle()
{
	POINT p;
	GetCursorPos(&p);
	HWND hwnd = WindowFromPoint(p);
	char wnd_title[256];
	GetWindowTextA(hwnd, wnd_title, sizeof(wnd_title));
	return wnd_title;
}

int main()
{
	ShowWindow(GetConsoleWindow(), SW_HIDE);

	InterceptionContext context;
	InterceptionDevice device;
	InterceptionKeyStroke stroke;

	wchar_t hardware_id[500];

	raise_process_priority();

	context = interception_create_context();

	interception_set_filter(context, interception_is_keyboard, INTERCEPTION_FILTER_KEY_ALL);
	interception_set_filter(context, interception_is_mouse, INTERCEPTION_FILTER_MOUSE_WHEEL);

	int win_pressed = 0;
	unsigned short prev_code = 0;
	unsigned short prev_state = 0;

	while (interception_receive(context, device = interception_wait(context), (InterceptionStroke *)&stroke, 1) > 0)
	{
		clock_t begin_time;
		if (interception_is_keyboard(device))
		{
			InterceptionKeyStroke &keystroke = *(InterceptionKeyStroke *)&stroke;

			//if (keystroke.code == SCANCODE_ESC) break;
		}

		size_t length = interception_get_hardware_id(context, device, hardware_id, sizeof(hardware_id));
		wstring ws(hardware_id);
		string str(ws.begin(), ws.end());
		if (length > 0 && length < sizeof(hardware_id))
		{
			cout << stroke.code << endl;
			cout << stroke.information << endl;
			cout << stroke.state << endl;
			wcout << hardware_id << endl;
		}

		const string dell_quietkey = "HID\\VID_413C&PID_2106&REV_0101";
		const string thinkpad_keyboard = "ACPI\\VEN_LEN&DEV_0071";
		const string thinkpad_ultranav = "ACPI\\VEN_LEN&DEV_0034";

		const string root = "\"D:\\Valentin\\Documente\\Visual Studio 2015\\Projects\\keyboard_hooker\\x64\\Debug\\";



			#pragma region ThinkPad Keyboard and UltraNav

		if (stroke.code == 91 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 3 && prev_code == 91 && prev_state == 2) // Windows key has been depressed, key combination ended
		{
			win_pressed = 0;
			stroke.code = 91;
			stroke.state = 2;
			interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			stroke.code = 91;
			stroke.state = 3;
			interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			prev_code = stroke.code;
			prev_state = stroke.state;
		}
		else
		{

			prev_code = stroke.code;
			prev_state = stroke.state;

			string title = GetCursorWindowTitle();
			if (stroke.code == 1024 && !strcmp(str.c_str(), thinkpad_ultranav.c_str()) && title.find("Mozilla Firefox") != string::npos) // UltraNav faster scrolling
			{
				for (int i = 0; i < 10; i++)
					interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}

			if (stroke.code == 33 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 2) // Calculator -> Volume Down
			{
				stroke.code = 46;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 33 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}

			else if (stroke.code == 50 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 2) // Browser -> Volume Up
			{
				stroke.code = 48;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 50 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}


			else if (stroke.code == 91 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 2) // Lock -> Volume Mute
			{
				stroke.code = 91;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
				win_pressed = 1;
				begin_time = clock();
			}
			else if (stroke.code == 38 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 0) // Lock -> Volume Mute
			{
				if (win_pressed)
				{
					win_pressed = 0;
					float f = float(clock() - begin_time);
					cout << f << endl;
					if (f <= 25)
					{
						stroke.code = 32;
						stroke.state = 2;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						win_pressed = 0;
					}
					else
					{
						stroke.code = 91;
						stroke.state = 2;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						stroke.code = 38;
						stroke.state = 0;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						stroke.code = 38;
						stroke.state = 1;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						stroke.code = 91;
						stroke.state = 3;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						win_pressed = 0;
					}
				}
				else
					interception_send(context, device, (InterceptionStroke *)&stroke, 1);

			}




			else if (stroke.code == 18 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 0) // Folder -> Calculator
			{
				if (win_pressed)
				{
					win_pressed = 0;
					float f = float(clock() - begin_time);
					cout << f << endl;
					if (f <= 15)
					{
						stroke.code = 33;
						stroke.state = 2;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						stroke.code = 33;
						stroke.state = 3;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						stroke.code = 91;
						stroke.state = 3;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						win_pressed = 0;
					}
					else
					{
						stroke.code = 91;
						stroke.state = 2;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						stroke.code = 18;
						stroke.state = 0;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						stroke.code = 18;
						stroke.state = 1;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						stroke.code = 91;
						stroke.state = 3;
						interception_send(context, device, (InterceptionStroke *)&stroke, 1);
						win_pressed = 0;
					}
				}
				else
					interception_send(context, device, (InterceptionStroke *)&stroke, 1);

			}


			else if (stroke.code == 55 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 2)
			{
				system((root + "MonitorOff.exe\"").c_str());
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 55 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}


			else if (stroke.code == 82 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 2)
			{
				stroke.code = 42;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
				stroke.code = 55;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
				stroke.code = 55;
				stroke.state = 3;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
				stroke.code = 42;
				stroke.state = 3;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 82 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}




			else if (stroke.code == 56 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 2) // Screen Brightness Down
			{
				exec((root + "nircmd.exe\" changebrightness -10").c_str());
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 56 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}




			else if (stroke.code == 29 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 2) // Screen Brightness Up
			{
				exec((root + "nircmd.exe\" changebrightness +10").c_str());
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 29 && !strcmp(str.c_str(), thinkpad_keyboard.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}

			
#pragma endregion

			#pragma region Dell_Quiet_Key

			else if (stroke.code == 70 && !strcmp(str.c_str(), dell_quietkey.c_str()) == 1 && stroke.state == 0) // Mute -> Scroll Lock
			{
				stroke.code = 32;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 70 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 1)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}





			else if (stroke.code == 42 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 2) // Volume Down -> Print Screen
			{
				stroke.code = 46;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 42 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 55 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 2)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 55 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}




			else if (stroke.code == 29 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 4) // Volume Up -> Pause
			{
				stroke.code = 48;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 29 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 5)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 69 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 0)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 69 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 1)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}




			else if (stroke.code == 82 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 2) // Insert -> Print Screen
			{
				stroke.code = 42;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
				stroke.code = 55;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
				stroke.code = 55;
				stroke.state = 3;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
				stroke.code = 42;
				stroke.state = 3;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 82 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}



			else if (stroke.code == 93 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 2) // Menu -> Calculator
			{
				stroke.code = 33;
				stroke.state = 2;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
			else if (stroke.code == 93 && !strcmp(str.c_str(), dell_quietkey.c_str()) && stroke.state == 3)
			{
				stroke.code = 0;
				stroke.state = 0;
				interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}

#pragma endregion


			else
			{
				if (win_pressed)
				{
					/*unsigned short code = stroke.code;
					unsigned short state = stroke.state;
					stroke.code = 91;
					stroke.state = 2;
					interception_send(context, device, (InterceptionStroke *)&stroke, 1);
					stroke.code = code;
					stroke.state = state;
					interception_send(context, device, (InterceptionStroke *)&stroke, 1);
					stroke.code = 91;
					stroke.state = 3;
					interception_send(context, device, (InterceptionStroke *)&stroke, 1);*/
					win_pressed = 0;
				}
				else
					interception_send(context, device, (InterceptionStroke *)&stroke, 1);
			}
		}
	}

	interception_destroy_context(context);

	return 0;
}