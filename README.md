Keyboard Hooker

This project uses the Inception library in order to configure different keybaords attached to a system according to user taste.
Currently, it has no UI, no configurable options. It just works for the ThinkPad W541, and Dell QuietKey Keybaord, remapping some keys to my taste. It is a good example showcasing how to remap keys only on certain keyboard.
There is no plan at the moment for an UI. In order to close the app, kill it from Task Manager.
It works on elevated applications even if not elevated, due to the fact that it uses a driver to process all the key changes.
Written in C++ for maximum scalability and performance. Candidate as replacement for ThinkPad Special Keys Remap (C# based), does all its functionality, and in a better way, now testing all the functionality.
